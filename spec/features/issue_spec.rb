feature 'Visitor can create new issue', js: true do

  before :all do
    @user_name = File.read('./spec/support/user.txt')
    @project_name = "#{@user_name} new project with issues"
    @project_page = project_page
    @issue_page = issue_page
    @issue_num

    login_user(@user_name)
    create_project(@project_name)
    logout_user
  end

  before :each do
    login_user(@user_name)
    visit '/projects'
    @project_page.header.search_box.select(@project_name).click
  end

  scenario 'Visitor can see all field at issue page for newly created project' do
    expect(@project_page).to have_content @project_name
    expect(@project_page.current_url).to include @user_name
    expect(@project_page.header).to have_issues
    expect(@project_page.header).to have_new_issue
    @project_page.header.new_issue.click
    expect(@issue_page).to have_content "New issue"
    expect(@issue_page).to have_tracker_selector
    expect(@issue_page).to have_subject
    expect(@issue_page).to have_description
    expect(@issue_page).to have_status
    expect(@issue_page).to have_priority
    expect(@issue_page).to have_submit_button
    @project_page.header.issues.click
    expect(@issue_page).to have_content "Issues"

  end

  scenario 'Visitor successfully can create new issue' do
    @project_page.header.new_issue.click
    create_new_bug(@user_name)
    @issue_num = @issue_page.bug_num.text[/\d+/]
    expect(@issue_page.current_url).to include "issues/#{@issue_num}"
    expect(@issue_page).to have_content "Issue ##{@issue_num} created."
  end

  scenario 'Visitor can see created issue in Issues tab' do
    @project_page.header.issues.click
    expect(@issue_page.body).to have_content @issue_num
  end
end
