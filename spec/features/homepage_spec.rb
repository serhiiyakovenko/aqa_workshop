feature 'Visitor visit homepage', js: true do

  before :all do
    @basic_page = basic_page
    @login_page = login_page
    @registration_page = registration_page
  end

  before :each do
    visit '/'
  end

  scenario 'Visitor successfully visits homepage' do
    expect(@basic_page.current_url).to include 'http://demo.redmine.org/'
    expect(@basic_page).to have_content 'Redmine demo'
  end

  scenario 'Visitor successfully visits sign in page' do
    @basic_page.top_menu.login_link.click
    expect(@login_page.current_url).to include '/login'
    expect(@login_page).to have_login_form
    expect(@login_page).to have_username
    expect(@login_page).to have_password
    expect(@login_page.login_button).to be_visible
  end

  scenario 'Visitor successfully visits registration page' do
    @basic_page.top_menu.registration_link.click
    expect(@registration_page.current_url).to include 'account/register'
    expect(@registration_page).to have_registration_form
    expect(@registration_page).to have_user_field
    expect(@registration_page).to have_password_field
    expect(@registration_page).to have_password_confirm_field
    expect(@registration_page).to have_first_name_field
    expect(@registration_page).to have_second_name_field
    expect(@registration_page).to have_mail_field
    expect(@registration_page.submit_button).to be_visible
  end

end
