# 1. Регистрируешь Юзера 1
# 2. Создаешь проект ??? I have skipped it as it same as 7, but useless
# 3. Логаут
# 4. Регистрируешь Юзера 2
# 5. Логаут
# 6. Логинишь Юзера 1
# 7. Создаешь проект
# 8. Создаешь новый Тикет
# 9. Назначаешь на Тикет Юзера 2
# 10. Логаут
# 11. Логин Юзером 2
# 12. Трекаешь время на Тикет
# 13. Закрываешь Тикет
# 14. Логаут
# 15. Логин Юзером 1
# 16. Закрываешь проект


feature 'Serega can make his test assigment', js: true do
  before :all do
    @basic_page = basic_page
    @login_page = login_page
    @registration_page = registration_page
    @my_page = my_page
    @project_page = project_page
    @issue_page = issue_page
    @admin = "admin" + DateTime.now.strftime('%d%m%H%M%S')
    @dev = "dev" + DateTime.now.strftime('%d%m%H%M%S')
    @project_name = "#{@admin} new project"
    puts @admin
    puts @dev
  end

  before :each do
    visit '/'
  end

  scenario 'Register admin petrovi4' do
    register_user(@admin,"Admin",@admin)
    logout_user
  end

  scenario 'Register dev Artem' do
    register_user(@dev,"Artem",@dev)
    logout_user
  end

  scenario 'Admin create new project with new ticket' do
    login_user(@admin)
    create_project(@project_name)
    expect(@project_page).to have_content "Successful creation."
    add_developer_to_project(@dev)
    @project_page.header.new_issue.click
    create_new_bug("NOTHING WORKSAAA111!1!!",@dev)
    logout_user
  end

  scenario 'Dev has solved issue with some tracted time and close ticket' do
    login_user(@dev)
    set_work_time("1","Development")
    expect(@issue_page).to have_content "Successful creation."
    close_ticket
  end

  scenario 'Admin see closed ticket and close project' do
    login_user(@admin)
    @basic_page.top_menu.mypage.click
    expect(@my_page.reported_issues).to have_content "(Closed)"
    close_project
    expect(@project_page).to have_content "This project is closed and read-only."
  end


end
