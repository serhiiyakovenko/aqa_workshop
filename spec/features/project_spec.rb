feature 'Visitor can create new project', js: true do

  before :all do
    @user_name = File.read('./spec/support/user.txt')
    @project_name = "#{@user_name} new project"
    @project_page = project_page
    @identifier
  end

  before :each do
    login_user(@user_name)
    visit '/projects'
  end

  scenario 'User can see all fields for new project' do
    expect(@project_page.header).to have_search_field
    @project_page.creation_project_button.click
    expect(@project_page).to have_new_project_form
    expect(@project_page).to have_project_name
    expect(@project_page).to have_project_description
    expect(@project_page).to have_project_identifier
    expect(@project_page.submit_button).to be_visible
  end

  scenario 'User can create new project' do
    create_project(@project_name)
    @identifier = @project_page.project_identifier.value
    expect(@project_page).to have_content "Successful creation."
  end

  scenario 'User can find created project' do
    #expect(@project_page).to have_select('project_quick_jump_box', with_options: [@project_name])
    expect(@project_page.header.search_box).to have_content @project_name
    @project_page.header.search_field.set(@project_name).send_keys :enter
    expect(@project_page.current_url).to include "#{@identifier}"
    expect(@project_page.results).to have_content @project_name
  end

end
