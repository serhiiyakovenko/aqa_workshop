feature 'Visitor is able to use authentication', js: true do
  before :all do
    @user_name = "syak" + DateTime.now.strftime('%d%m%H%M%S')
    @basic_page = basic_page
    puts "User_name is #{@user_name}"
  end

  scenario 'Visitior successfully registered' do
    register_user(@user_name)
    expect(@basic_page.current_url).to include '/my/account'
    expect(@basic_page.top_menu).to have_account
    expect(@basic_page).to have_content 'Your account has been activated. You can now log in.'
    expect(@basic_page).to have_content @user_name
    expect(@basic_page.top_menu).to have_logout_link
  end

  scenario 'Visitor successfully logs in' do
    login_user(@user_name)
    expect(@basic_page.top_menu).to have_mypage
    expect(@basic_page.top_menu).to have_logout_link
    expect(@basic_page).to have_content @user_name
  end

  scenario 'Visitor successfully logs out' do
    login_user(@user_name)
    logout_user
    expect(@basic_page.top_menu).to have_login_link
    expect(@basic_page.top_menu).not_to have_mypage
    expect(@basic_page.top_menu).not_to have_content @user_name
  end

end
