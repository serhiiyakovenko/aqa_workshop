module FeatureHelper

  def register_user(user_name,first_name = "Serhii",second_name = "Yakovenko",password = "Qwerty1")
    visit '/'
    basic_page.top_menu.registration_link.click
    registration_page.user_field.set user_name
    registration_page.password_field.set password
    registration_page.password_confirm_field.set password
    registration_page.first_name_field.set first_name
    registration_page.second_name_field.set second_name
    registration_page.mail_field.set user_name + "@gmail.com"
    registration_page.submit_button.click

    File.write('./spec/support/user.txt', user_name)

  end

  def login_user(user_name,password = "Qwerty1")
    visit '/login'
    login_page.username.set user_name
    login_page.password.set password
    login_page.login_button.click
  end

  def logout_user
    basic_page.top_menu.logout_link.click
  end

  def create_project(project_name,descr = "Some test descr")
    visit '/projects'
    project_page.creation_project_button.click
    project_page.project_name.set project_name
    project_page.project_description.set descr
    project_page.submit_button.click
  end

  def add_developer_to_project(user_name)
    project_page.members.click
    project_page.new_members.click
    project_page.member_search.set user_name
    project_page.wait_for_member_search_result
    project_page.member_search_result.click
    project_page.developer.click
    project_page.member_submit.click
  end

  def create_new_bug(name,assignee = "")
    issue_page.subject.set name
    issue_page.description.set name + " some test desc"
    if assignee != ""
      issue_page.assign_to.select(assignee)
    end
    issue_page.submit_button.click
  end

  def set_work_time(time,activity)
    basic_page.top_menu.mypage.click
    my_page.bug.click
    issue_page.time_track.click
    issue_page.input_work_time_h.set time
    if activity == "Development" || activity == "Design"
      issue_page.time_activity.select(activity)
    end
    issue_page.submit_button.click
  end

  def close_ticket
    issue_page.edit_button.click
    issue_page.status.select("Closed")
    issue_page.submit_button.click
  end

  def close_project
    my_page.project.click
    project_page.close_project.click
    project_page.accept_alert
  end
end
