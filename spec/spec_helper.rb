require 'rspec'
require 'selenium-webdriver'
require 'capybara'
require 'capybara/rspec'
require 'site_prism'

require_relative 'support/feature_helper'
require_relative '../pages/_app_site'

include FeatureHelper

Capybara.app_host = 'http://demo.redmine.org/'

RSpec.configure do |config|
  config.include DemoAppSite
  config.before :all do

    #setting Capybara driver
    Capybara.default_driver = :selenium
    Capybara.register_driver :selenium do |app|
      Capybara::Selenium::Driver.new(app, browser: :chrome)
    end
    #Make fullscreen
    Capybara.page.driver.browser.manage.window.maximize
  end

  config.after :all do
    Capybara.reset_sessions!
  end
end
