class TopMenuSection < SitePrism::Section
  element :account, 'a.my-account'
  element :logout_link, 'a.logout'
  element :login_link, 'a.login'
  element :registration_link, 'a.register'
  element :mypage, 'a.my-page'
  element :projects, 'a.projects'
end

class HeaderSection < SitePrism::Section
  element :search_box, '#project_quick_jump_box'
  element :search_field, '#q'
  element :new_issue, 'a.new-issue'
  element :issues, 'a.issues'
end

class BasicPage < SitePrism::Page
  set_url '/'

  section :header, HeaderSection, 'div#header'
  section :top_menu, TopMenuSection, "#top-menu"
end

class LoginPage < SitePrism::Page
  section :top_menu, TopMenuSection, "#top-menu"

  element :login_form, 'div#login-form'
  element :username, '#username'
  element :password, '#password'
  element :login_button, "input[name='login']"
end

class RegistrationPage < SitePrism::Page
  element :registration_form, '#new_user'
  element :user_field, '#user_login'
  element :password_field, '#user_password'
  element :password_confirm_field, '#user_password_confirmation'
  element :first_name_field, '#user_firstname'
  element :second_name_field, '#user_lastname'
  element :mail_field, '#user_mail'
  element :submit_button, "input[name='commit']"
end

class MyPage < SitePrism::Page
  section :header, HeaderSection, 'div#header'
  section :top_menu, TopMenuSection, "#top-menu"

  element :my_issues, 'div#list-left'
  element :bug, :xpath, '//td[1]/a'
  element :reported_issues, 'div#list-right'
  element :project, :xpath, '//td[2]/a'


end

class ProjectPage < SitePrism::Page

  section :header, HeaderSection, 'div#header'

  element :creation_project_button, 'a.icon.icon-add'
  element :new_project_form, '#new_project'
  element :project_name, '#project_name'
  element :project_description, '#project_description'
  element :project_identifier, '#project_identifier'
  element :submit_button, "input[name='commit']"
  element :results, 'dl#search-results'
  element :members, 'a#tab-members'
  element :new_members, 'a.icon.icon-add'
  element :member_search, '#principal_search'
  element :member_search_result, :xpath, '//*[@id="principals"]/label/input'
  element :developer, :xpath, '//*[@id="new_membership"]/fieldset[2]/div/label[2]/input'
  element :member_submit, '#member-add-submit'
  element :close_project, 'a.icon.icon-lock'
end

class IssuePage < SitePrism::Page

  section :top_menu, TopMenuSection, "#top-menu"

  element :tracker_selector, '#issue_tracker_id'
  element :subject, '#issue_subject'
  element :description, '#issue_description'
  element :status, '#issue_status_id'
  element :assign_to, '#issue_assigned_to_id'
  element :priority, '#issue_priority_id'
  element :submit_button, "input[name='commit']"
  element :notice, '#flash_notice'
  element :bug_num, :xpath, '//*[@id="content"]/h2'
  element :edit_button, :xpath, '//*[@id="content"]/div[2]/a[1]'
  element :time_track, :xpath,  '//*[@id="content"]/div[1]/a[2]'
  element :input_work_time_h, '#time_entry_hours'
  element :time_activity, '#time_entry_activity_id'
end
