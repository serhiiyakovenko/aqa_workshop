module DemoAppSite

  def basic_page
    BasicPage.new
  end

  def login_page
    LoginPage.new
  end

  def registration_page
    RegistrationPage.new
  end

  def my_page
    MyPage.new
  end

  def project_page
    ProjectPage.new
  end

  def issue_page
    IssuePage.new
  end

end

World(DemoAppSite)