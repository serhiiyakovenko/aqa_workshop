
Feature: API is awesome
  As a user of Reqres
  I want to be able to use API
  So I save a lot of time

  Scenario Outline: I send different requests and get different response
      Given I have valid API key
       When I use <method> for <url>
       Then I see response code <status_code>

       Examples:
        | method | url       | status_code |
        | GET    | users     | 200         |
        | GET    | users/2   | 200         |
        | GET    | unknown   | 200         |
        | GET    | users/23  | 404         |
        | DELETE | users/2   | 204         |
