Given(/^I have valid API key$/) do
  #TO DO
end

When(/^I use (GET|DELETE) for (.*)$/) do |method, url|
  @stock = case method
           when 'GET', 'DELETE'
             get_response(method, url)
           else
             #skip for now
           end
end

When(/^I see response code (\d+)$/) do |status_code|
  expect(@stock.code).to eq status_code
end

When(/^I see correct response body$/) do |code|
  expect(@stock.code).to eq 200
end
