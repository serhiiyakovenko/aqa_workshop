When(/^I visit Redmine page$/) do
  basic_page.load
end

When(/^I create user: '([^']*)'$/) do |user|
  if user == 'Admin'
    register_user(@admin,"Admin",@admin)
  elsif user == 'Developer'
    register_user(@dev,"Artem",@dev)
  end
  logout_user
end

When(/^Admin create new project with new ticket and assign it to developer$/) do
  login_user(@admin)
  create_project(@project_name)
  expect(project_page).to have_content "Successful creation."
  add_developer_to_project(@dev)
  project_page.header.new_issue.click
  create_new_bug("NOTHING WORKSAAA111!1!!",@dev)
  logout_user
end

Then(/^Developer has solved issue with some traced time and close ticket$/) do
  login_user(@dev)
  set_work_time("1","Development")
  expect(issue_page).to have_content "Successful creation."
  close_ticket
  logout_user
end

And(/^Admin see closed ticket and close project$/) do
  login_user(@admin)
  basic_page.top_menu.mypage.click
  expect(my_page.reported_issues).to have_content "(Closed)"
  close_project
  expect(project_page).to have_content "This project is closed and read-only."
end