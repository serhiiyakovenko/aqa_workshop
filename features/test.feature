@new_user
Feature: Redmine give possibility to create project and ticket
  As a Admin
  I want to be able to create a project and a ticket
  So Developer have task to do

  Scenario: Admin create new project and a ticket for Developer and close project on completion
    When I visit Redmine page
    Then I create user: 'Admin'
     And I create user: 'Developer'
    When Admin create new project with new ticket and assign it to developer
    Then Developer has solved issue with some traced time and close ticket
     And Admin see closed ticket and close project