require 'cucumber'
require 'rspec'
require 'selenium-webdriver'
require 'capybara/cucumber'
require 'site_prism'
require 'require_all'
require 'rest-client'

Capybara.app_host = 'http://demo.redmine.org'

require_all 'pages'

#setting Capybara driver
Capybara.default_driver = :selenium
Capybara.register_driver :selenium do |app|
  Capybara::Selenium::Driver.new(app, browser: :chrome)
end

Before do
  #Make fullscreen
  Capybara.page.driver.browser.manage.window.maximize
end

After do
  Capybara.reset_sessions!
end
